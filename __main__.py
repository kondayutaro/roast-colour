#!/usr/bin/env python3
import serial
import color


ser = serial.Serial('/dev/tty.usbmodem142201', 9600)
color_store: list[color.Color] = []

for i in range(10):
    readout = ser.readline().decode('utf8')
    color_to_store = color.Color()
    color_to_store.update_color(readout)
    color_store.append(color_to_store)
ser.close()

color_average = color.Color()
for element in color_store:
    color_average.kelvin += element.kelvin
    color_average.lux += element.lux
    color_average.red += element.red
    color_average.green += element.green
    color_average.blue += element.blue

color_average.kelvin = color_average.kelvin / len(color_store)
color_average.lux = color_average.lux / len(color_store)
color_average.red = color_average.red / len(color_store)
color_average.green = color_average.green / len(color_store)
color_average.blue = color_average.blue / len(color_store)

print(color_average.kelvin)
print(color_average.lux)
print('#%02x%02x%02x' % (round(color_average.red), round(color_average.green), round(color_average.blue)))
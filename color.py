class Color:
    def __init__(self) -> None:
        self.kelvin: float = 0
        self.lux: float = 0
        self.red: float = 0
        self.green: float = 0
        self.blue: float = 0
    
    def update_color(self, color_string: str):
        list = color_string.split(sep=',')
        self.kelvin, self.lux, self.red, self.green, self.blue = [ float(element) for element in list ]

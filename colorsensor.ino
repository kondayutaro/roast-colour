#include <Wire.h>
#include "Adafruit_TCS34725.h"

/* Example code for the Adafruit TCS34725 breakout library */

/* Connect SCL    to analog 5
   Connect SDA    to analog 4
   Connect VDD    to 3.3V DC
   Connect GROUND to common ground */

/* Initialise with default values (int time = 2.4ms, gain = 1x) */
// Adafruit_TCS34725 tcs = Adafruit_TCS34725();

/* Initialise with specific int time and gain values */
Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_700MS, TCS34725_GAIN_1X);

void setup(void) {
  Serial.begin(9600);
}

void loop(void) {
  float r, g, b;
  uint16_t colorTemp, lux;

  tcs.getRGB(&r, &g, &b);
  colorTemp = tcs.calculateColorTemperature(r, g, b);
  lux = tcs.calculateLux(r, g, b);

  Serial.print(colorTemp, DEC);
  Serial.print(',');
  Serial.print(lux, DEC);
  Serial.print(',');
  Serial.print(r, DEC);
  Serial.print(',');
  Serial.print(g, DEC);
  Serial.print(',');
  Serial.print(b, DEC);
  Serial.println(" ");
}
